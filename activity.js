//express package was imported as express
const express = require("express");

//invoked express package to create a server/api and save it in a variable which we can refer to later to create routes
const app = express();

//variable for port assisgnment
const port = 3000;

app.use(express.json());

//by applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));


//CREATE A ROUTE IN EXPRESS
	//express has methods corresponding to each HTTP Method

//1
app.get("/home",(req,res)=>{

	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Welcome to the home page")
});

//2
let users = [
  {
    username: "johndoe",
    password: "johndoe123"
  },
  {
    username: "janedoe",
    password: "jane321"
  },
  {
    username: "joedee",
    password: "joedee456"
  }
];

app.get('/users', (req, res) => {
  res.send(users);
});

app.delete('/delete-user', (req, res) => {
  let message;
  let userIndex = -1;

  for(let i = 0; i < users.length; i++) {
    if (req.body.username === users[i].username) {
      userIndex = i;
      break;
    }
  }

  if (userIndex === -1) {
    message = `User with username ${req.body.username} does not exist`;
  } else {
    users.splice(userIndex, 1);
    message = `User ${req.body.username} has been deleted`;
  }

  res.send(message);
});



//tells our server to listen to the port
app.listen(port,()=> console.log(`Server is running at port ${port}`))